"""Test cki_lib.cronjob."""
import datetime
import unittest
from unittest import mock

from freezegun import freeze_time

from cki_lib import cronjob


class TestRun(unittest.TestCase):
    """Test run method."""

    @staticmethod
    def test_check_and_run():
        """Test check_and_run method."""
        jobs = [mock.Mock()]
        cronjob.check_and_run(jobs)
        jobs[0].assert_has_calls([mock.call.check_and_run()])


class TestCronJob(unittest.TestCase):
    """Test CronJob class."""

    def setUp(self):
        """Initialize."""
        self.job = cronjob.CronJob()

    def test_run(self):
        """Test run method."""
        self.assertRaises(NotImplementedError, self.job.run)

    def test__run(self):
        # pylint: disable=protected-access
        """Test _run method."""
        self.job.run = mock.Mock()
        self.job._running = mock.Mock()
        self.job._run(last_run_datetime=0)

        self.assertTrue(self.job.run.called)
        self.job._running.assert_has_calls([
            mock.call.clear()
        ])

    @freeze_time("2010-01-02 00:00:00")
    def test_next(self):
        # pylint: disable=protected-access
        """Test _next method with different schedules."""
        cases = [
            ('*/5 * * * *', [(0, 5, 0), (0, 10, 0), (0, 15, 0)]),
            ('*/10 * * * *', [(0, 10, 0), (0, 20, 0), (0, 30, 0)]),
            ('0 */2 * * *', [(2, 0, 0), (4, 0, 0), (6, 0, 0)]),
        ]

        for schedule, times in cases:
            self.job.schedule = schedule
            self.job._last_run_datetime = datetime.datetime.now()

            for expected_next in times:
                expected_next = datetime.datetime(
                    2010, 1, 2,
                    *expected_next,
                    tzinfo=datetime.timezone.utc
                )
                self.assertEqual(expected_next, self.job._next())
                self.job._last_run_datetime = expected_next

    @freeze_time("2010-01-02 00:00:00")
    def test_check_and_run_succesive(self):
        # pylint: disable=protected-access
        """
        Test check_and_run method.

        Two succesive calls do not call check_and_run twice as time
        did not pass.
        """
        self.job.run = mock.Mock()

        self.job.check_and_run()

        self.assertTrue(self.job.run.called)
        self.job.run.assert_called_with(last_run_datetime=None)
        self.assertEqual(
            self.job._last_run_datetime,
            datetime.datetime(2010, 1, 2, 0, 0, tzinfo=datetime.timezone.utc)
        )

        self.job.run.reset_mock()

        self.job.check_and_run()
        self.assertFalse(self.job.run.called)

    def test_check_and_run_succesive_later(self):
        """
        Test check_and_run method.

        Successive calls do call check_and_run if time passed.
        """
        self.job.schedule = '*/5 * * * *'
        self.job.run = mock.Mock()

        with freeze_time("2010-01-02 00:00:00"):
            self.job.check_and_run()
            self.assertTrue(self.job.run.called)
            self.job.run.assert_called_with(last_run_datetime=None)

        with freeze_time("2010-01-02 00:05:00"):
            # 5 minutes passed
            self.job.run.reset_mock()
            self.job.check_and_run()
            self.assertTrue(self.job.run.called)
            self.job.run.assert_called_with(
                last_run_datetime=datetime.datetime(
                    2010, 1, 2, 0, 0, tzinfo=datetime.timezone.utc
                )
            )

        with freeze_time("2010-01-02 00:10:00"):
            # 5 minutes passed
            self.job.run.reset_mock()
            self.job.check_and_run()
            self.assertTrue(self.job.run.called)
            self.job.run.assert_called_with(
                last_run_datetime=datetime.datetime(
                    2010, 1, 2, 0, 5, tzinfo=datetime.timezone.utc
                )
            )

    def test_check_and_run_lock_flags(self):
        # pylint: disable=protected-access
        """
        Test check_and_run sets the _running flag.

        Successive calls when the previous run did not finish raise error.
        """
        self.job._run = mock.Mock()

        self.job.check_and_run()

        self.assertTrue(self.job._running.is_set())

    def test_check_and_run_single_run_lock(self):
        # pylint: disable=protected-access
        """
        Test check_and_run does not run more than once.

        Successive calls when the previous run did not finish raise error.
        """
        self.job.schedule = '*/5 * * * *'
        self.job._run = mock.Mock()

        with freeze_time("2010-01-02 00:00:00"):
            self.job.check_and_run()

        self.assertTrue(self.job._running.is_set())

        with freeze_time("2010-01-02 00:05:00"):
            with self.assertLogs(cronjob.LOGGER, 'INFO') as logger:
                self.job.check_and_run()
                self.assertEqual(
                    [
                        "INFO:cki.cki_lib.cronjob:"
                        "Job 'CronJob' is scheduled to run but it's still running.",
                        "ERROR:cki.cki_lib.cronjob:"
                        "BackOff limit exceeded for job 'CronJob' (current: 1, limit: 0)",
                    ],
                    logger.output
                )

    def test_check_and_run_single_run_lock_back_off_limit(self):
        # pylint: disable=protected-access
        """
        Test check_and_run does not run more than once.

        Successive calls when the previous run did not finish raise error
        if the back_off_count is higher than the limit.
        """
        self.job.schedule = '*/5 * * * *'
        self.job.back_off_limit = 2
        self.job._run = mock.Mock()

        with freeze_time("2010-01-02 00:00:00"):
            self.job.check_and_run()

        self.assertTrue(self.job._running.is_set())

        with self.assertLogs(cronjob.LOGGER, 'INFO') as logger:
            with freeze_time("2010-01-02 00:05:00"):
                self.job.check_and_run()
                self.assertEqual(self.job._back_off_counter, 1)

            with freeze_time("2010-01-02 00:10:00"):
                self.job.check_and_run()
                self.assertEqual(self.job._back_off_counter, 2)

            with freeze_time("2010-01-02 00:15:00"):
                self.job.check_and_run()
                self.assertEqual(self.job._back_off_counter, 3)

                self.assertEqual(
                    [
                        "INFO:cki.cki_lib.cronjob:"
                        "Job 'CronJob' is scheduled to run but it's still running.",
                        "INFO:cki.cki_lib.cronjob:"
                        "Job 'CronJob' is scheduled to run but it's still running.",
                        "INFO:cki.cki_lib.cronjob:"
                        "Job 'CronJob' is scheduled to run but it's still running.",
                        "ERROR:cki.cki_lib.cronjob:"
                        "BackOff limit exceeded for job 'CronJob' (current: 3, limit: 2)",
                    ],
                    logger.output
                )

    def test_check_and_run_backoff_only_after_period(self):
        # pylint: disable=protected-access
        """Make sure the back_off_count only increases after a task period."""
        self.job.schedule = '*/5 * * * *'
        self.job._running.set()

        with freeze_time("2010-01-02 00:05:00"):
            self.job.check_and_run()
            self.assertEqual(self.job._back_off_counter, 1)
            self.job.check_and_run()
            self.assertEqual(self.job._back_off_counter, 1)

    def test_check_and_run_backoff_count_reset(self):
        # pylint: disable=protected-access
        """Test check_and_run resets _back_off_counter after successful run."""
        self.job.schedule = '*/5 * * * *'
        self.job._run = mock.Mock()

        with freeze_time("2010-01-02 00:00:00"):
            self.job.check_and_run()

        self.assertTrue(self.job._running.is_set())

        with freeze_time("2010-01-02 00:05:00"):
            self.job.check_and_run()
            self.assertEqual(self.job._back_off_counter, 1)

        self.job._running.clear()

        with freeze_time("2010-01-02 00:10:00"):
            self.job.check_and_run()
            self.assertEqual(self.job._back_off_counter, 0)

#!/bin/bash

set -Eeuo pipefail
# no shopt -s inherit_errexit as this needs to run on the ancient Bash in RHEL7
# instead, has 'set -e; ' below as recommended by shellcheck

# pass the packages to test (without tests directory) as args to the script

. cki_utils.sh

function did_coverage_decrease() {
    local pipelines_json pipeline_id pipeline_json old_coverage new_coverage=${1}
    shift
    echo -n "  Getting last successful pipeline... "
    # shellcheck disable=SC2154,SC2310
    if ! cki_is_true "${CKI_COVERAGE_ENABLED:-true}"; then
        cki_echo_yellow "skipped because CKI_COVERAGE_ENABLED is false"
        return 1
    fi
    if ! [[ -v CI_MERGE_REQUEST_PROJECT_ID ]]; then
        cki_echo_yellow "skipped because not in CI MR job"
        return 1
    fi
    if [[ -v CI_MERGE_REQUEST_TARGET_BRANCH_SHA ]]; then
        # for merged result pipelines, the change of the MR is introduced by
        # the merge commit, so take the coverage of the last target branch
        # commit right before the merge commit as the reference
        # shellcheck disable=SC2154
        pipelines_json=$(curl -Ss "${CI_API_V4_URL}/projects/${CI_MERGE_REQUEST_PROJECT_ID}/pipelines?sha=${CI_MERGE_REQUEST_TARGET_BRANCH_SHA}&status=success")
    else
        # shellcheck disable=SC2154
        pipelines_json=$(curl -Ss "${CI_API_V4_URL}/projects/${CI_MERGE_REQUEST_PROJECT_ID}/pipelines?ref=${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}&status=success")
    fi
    pipelines_length=$(jq '. | length' <<< "${pipelines_json}")
    if (( pipelines_length == 0 )); then
        cki_echo_yellow "skipped because no successful pipeline found"
        return 1
    fi
    pipeline_id=$(jq '.[0].id' <<< "${pipelines_json}")
    cki_echo_green " ${pipeline_id}"
    echo -n "  Getting old coverage... "
    pipeline_json=$(curl -Ss "${CI_API_V4_URL}/projects/${CI_MERGE_REQUEST_PROJECT_ID}/pipelines/${pipeline_id}")
    old_coverage=$(jq -r '.coverage // "0.00"' <<< "${pipeline_json}")
    # round up with a precision of two decimals, eg 65.9518 -> 65.96
    if python -c "import math; exit(math.ceil(${new_coverage} * 100) / 100 < ${old_coverage})"; then
        cki_echo_green "${old_coverage}%"
        return 1
    else
        if [[ -v CI_MERGE_REQUEST_TARGET_BRANCH_SHA ]]; then
            # for merged result pipelines, also check whether lines of code
            # increased, and only fail the coverage check in that case
            if git show -m --numstat --format='' HEAD -- "$@" | \
                awk '{ added += $1; removed += $2 } END { exit added - removed > 0 }'; then
                cki_echo_yellow "${old_coverage}% higher than new ${new_coverage}%, but line count did not increase"
                return 1
            else
                cki_echo_red "${old_coverage}% higher than new ${new_coverage}%, and line count increased"
                return 0
            fi
        else
            cki_echo_red "${old_coverage}% higher than new ${new_coverage}%"
            return 0
        fi
    fi
}

cki_say "preparing"

PACKAGES=(
    flake8
    autopep8
    pydocstyle
    "isort[colors]"
    pylint
    pytest
    coverage
)

python_type=$(type -P python3)
if [[ ${python_type} = /usr* ]]; then
    PIP_INSTALL=(python3 -m pip install --user)
else
    PIP_INSTALL=(python3 -m pip install)
fi

"${PIP_INSTALL[@]}" "${PACKAGES[@]}"

overrides_str=$(compgen -A variable | grep '_pip_url$' || true)
readarray -t overrides <<< "${overrides_str}"
for override in "${overrides[@]}"; do
    if [[ -z ${override} ]] || [[ ${!override} != git+https://* ]]; then
        continue
    fi
    pip_url=${!override}
    package_name=$(set -e; cki_git_clean_url "${pip_url%@*}")
    package_name=${package_name%.git/}
    package_name=${package_name##*/}
    cki_echo_yellow "Found ${package_name} override: ${pip_url}"
    python3 -m pip uninstall -y "${package_name}"
    "${PIP_INSTALL[@]}" "${pip_url}"
done

MAX_LINE_LENGTH=100
if [[ -f setup.cfg ]] && grep -q 'max-line-length=' setup.cfg; then
    MAX_LINE_LENGTH=$(sed -n s/max-line-length=//p setup.cfg | head -n 1)
    cki_echo_yellow "Overriding maximum line length with ${MAX_LINE_LENGTH}"
fi

_isort=(
    isort
    --color
    --force-single-line-imports
    --force-sort-within-sections
    --line-length "${MAX_LINE_LENGTH}"
    --skip-glob '.*'
    --skip-glob '**/migrations'
)

cki_say "linting"

FAILED=()

if [[ ${1:-} = "--fix" ]]; then
    shift

    # Try to fix issues in ALL Python files via autopep8 and isort

    cki_echo_yellow "Running autopep8"
    if ! autopep8 --max-line-length "${MAX_LINE_LENGTH}" --exclude "migrations" --in-place --recursive .; then
        FAILED+=(autopep8)
    fi

    cki_echo_yellow "Running isort"
    if ! "${_isort[@]}" .; then
        FAILED+=(isort)
    fi
else
    # Check ALL Python files with flake8 (which includes pycodestyle), pydocstyle and isort

    cki_echo_yellow "Running flake8"
    if ! flake8 --max-line-length "${MAX_LINE_LENGTH}" --exclude ".*,migrations" .; then
        FAILED+=(flake8)
        cki_echo_red "  Run cki_lint.sh --fix to run autopep8"
    fi

    cki_echo_yellow "Running pydocstyle"
    if ! pydocstyle --match-dir='^(?!(\.|migrations)).*'; then
        FAILED+=(pydocstyle)
    fi

    cki_echo_yellow "Running isort"
    if ! "${_isort[@]}" --check --diff .; then
        FAILED+=(isort)
        cki_echo_red "  Run cki_lint.sh --fix to update import order"
    fi

    # If mypy is installed (eg. via the dev extra), run mypy check
    if type mypy > /dev/null 2>&1; then
        cki_echo_yellow "Running mypy"
        if  ! mypy --strict; then
            FAILED+=(mypy)
        fi
    fi

    # Only run pylint and coverage for the specified packages

    cki_echo_yellow "Running pylint"
    pylint_args=(
        --max-line-length "${MAX_LINE_LENGTH}"
        --load-plugins pylint.extensions.no_self_use
    )
    if [[ -f setup.cfg ]]; then
        pylint_args+=(--rcfile setup.cfg)
    fi
    if ! pylint "${pylint_args[@]}" "$@"; then
        FAILED+=(pylint)
    fi

    if [[ -f tests/__init__.py ]]; then
        cki_say "testing"
        cki_echo_yellow "Running pytests under coverage"
        if ! coverage run --source "$(IFS=,; echo "$*")" --branch -m pytest --junitxml=coverage/junit.xml --ignore inttests/ --color=yes -v -r s; then
            FAILED+=(pytest)
        fi

        cki_echo_yellow "Generating coverage reports"
        coverage report -m || true
        coverage html -d coverage/ || true
        coverage xml -o coverage/coverage.xml || true

        cki_echo_yellow "Total coverage and trend"
        new_coverage=$(coverage json -o - | jq .totals.percent_covered)
        echo "  COVERAGE: ${new_coverage}%"

        did_coverage_decrease "${new_coverage}" "$@" &
        wait $! && coverage_decreased=1 || coverage_decreased=0
        if [[ ${coverage_decreased} = 1 ]]; then
            commit_message="$(git log -n 1 --format=%B "${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA:-HEAD}" || true)"
            if [[ "${commit_message}" != *"[skip coverage check]"* ]]; then
                FAILED+=(coverage)
                cki_echo_red "  Include [skip coverage check] in the commit message to skip this check"
            fi
        fi
    fi
fi

if (( ${#FAILED[@]} > 0 )); then
    cki_echo_red "Failed linting steps: ${FAILED[*]}"
    exit 1
fi

